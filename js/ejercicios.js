console.log("carga correcta");
//Comprobación de que se carga correctamente ^

$(document).on("click", "div.circles", function () {
    $(this).toggleClass("gris"); //ToggleClass cambia clase, gris a original y original a gris con cada click
});

//Ejercicio 1 JQuery ^

$(document).on("click", "button.buttonRed", function () {
    $("div.circles2").css("background-color", "red");
});

$(document).on("click", "button.buttonNoRed", function () {
    $("div.circles2").css("background-color", "orange");
});

//Ejercicio 1b JQuery ^

$("button.copy").click(function () {
    copiedText = $("#copyText").val();
    $("#copiedText").val(copiedText);
});

//Ejercicio 2 JQuery ^

$("button.copy").click(function () {
    copiedText = $("#copyText").val();
    $("th").text(copiedText);
});

//Ejercicio 3 JQuery ^

$("button.add").click(function () {
    copiedText = $("#copyText").val();
    $("th").append(copiedText);
});

//Ejercicio 3b JQuery ^

$("#numberInput").attr('disabled', 'disabled');

$(function () {
    $("#numberInput").val("0");
});

var counter = $("#numberInput").val();
$("button.plus").on("click", function () {
    counter++;
    $("#numberInput").val(counter);
});

var counter = $("#numberInput").val();
$("button.minus").on("click", function () {
    counter--;
    $("#numberInput").val(counter);
});

//Ejercicio 4 JQuery ^ 

$("#numberInput_2").attr('disabled', 'disabled');

$(function () {
    $("#numberInput_2").val("0");
});

var counter = $("#numberInput_2").val();
$("button.plus_2").on("click", function () {
    if (counter == 10) {
        $("#numberInput_2").val(counter);
    } else {
        counter++;
        $("#numberInput_2").val(counter);
    }
});

var counter = $("#numberInput_2").val();
$("button.minus_2").on("click", function () {
    if (counter == 0) {
        $("#numberInput_2").val(counter);
    } else {
        counter--;
        $("#numberInput_2").val(counter);
    }
});

//Ejercicio 4b JQuery ^

$("button.button_calcula").on("click", function () {
    counter1 = Math.floor(Math.random() * 49);
    $("#circle_1").text(counter1);
    counter2 = Math.floor(Math.random() * 49);
    $("#circle_2").text(counter2);
    counter3 = Math.floor(Math.random() * 49);
    $("#circle_3").text(counter3);
});

//Ejercicio 5 JQuery ^

var counter = [$("#button_list1").text(), $("#button_list2").text(), $("#button_list3").text(),
$("#button_list4").text(), $("#button_list5").text()];
$("#plus_list").on("click", function () {
    if (counter[0] == 16) {
        $("#button_list1").text(counter[0]);
        $("#button_list2").text(counter[1]);
        $("#button_list3").text(counter[2]);
        $("#button_list4").text(counter[3]);
        $("#button_list5").text(counter[4]);
    } else {
        counter[0]++;
        counter[1]++;
        counter[2]++;
        counter[3]++;
        counter[4]++;
        $("#button_list1").text(counter[0]);
        $("#button_list2").text(counter[1]);
        $("#button_list3").text(counter[2]);
        $("#button_list4").text(counter[3]);
        $("#button_list5").text(counter[4]);
    }
});

$("#minus_list").on("click", function () {
    if (counter[0] == 1) {
        $("#button_list1").text(counter[0]);
        $("#button_list2").text(counter[1]);
        $("#button_list3").text(counter[2]);
        $("#button_list4").text(counter[3]);
        $("#button_list5").text(counter[4]);
    } else {
        counter[0]--;
        counter[1]--;
        counter[2]--;
        counter[3]--;
        counter[4]--;
        $("#button_list1").text(counter[0]);
        $("#button_list2").text(counter[1]);
        $("#button_list3").text(counter[2]);
        $("#button_list4").text(counter[3]);
        $("#button_list5").text(counter[4]);
    }
});

//Ejercicio 6 JQuery ^

$("div.switch").click(function () {
    $("div.switch img").toggle();
    $("img.lightbulb_on").fadeToggle(3000);
    $("img.lightbulb_on2").css("opacity", $(this).val(0));
});

$(document).on("input", "#myRange", function () {
    console.log("funciona!" + $(this).val());
    $("img.lightbulb_on2").css("opacity", $(this).val()/100);
});

//Ejercicio 7 JQuery ^

$("div.banana").click(function () {
    $("div.banana img").toggle();
});

//Ejercicio 8 JQuery ^

$(document).on("click", "button.boton1", function () {
    var algo = $("<h1>").text("Poner algo...");
    $("div.div1").append(algo);
});

//Ejercicio 9 JQuery ^

var lista=["Segundo","Tercero","Cuarto","Quinto", "Sexto"];
$(document).on("click", "button.boton2", function () {
    console.log("¡Funciona!");
    $(lista).each(function(index,item) {
        $("#listaItems").append("<li>"+[item]+"</li>");
    });
});

//Ejercicio 10 JQuery ^

var listaTelefonos = [{
    nombre: "ana",
    tel: "android"
},
{
    nombre: "javi",
    tel: "iphone"
},
{
    nombre: "carmen",
    tel: "android"
},
{
    nombre: "rubén",
    tel: "android"
},
];

$(document).on("click", "button.boton3", function () {
    console.log("¡Funciona!");
    var trHTML = '';
    $(listaTelefonos).each(function(i, cliente) {
        trHTML = "<tr><td>" + cliente.nombre + "</td><td>" + cliente.tel + "</td></tr>";
        $('#tbody_class').append(trHTML);
    });
    });

//Ejercicio 11 JQuery ^
var androides = 0;
var iphones = 0;
var otros = 0;
$(document).on("click", "button.boton4", function () {
    console.log("¡Funciona!");
$.getJSON("Json/clientes.json", function (data) {
    data.clientes.forEach(function (cliente) {
        console.log(cliente.nombre);
        trHTML = "<tr><td>" + cliente.id + "</td><td>" + cliente.nombre + "</td><td>" 
        + cliente.tel + "</td></tr>";
        if (cliente.tel == "android") {
            androides++;
        }
        else if (cliente.tel == "iphone") {
            iphones++;
        }else {
            otros++;
        }
    
        $("#tbody_class2").append(trHTML);
        $("button.boton4").on("click",function() {
            $(this).prop("disabled",true);
        });
    });
    todos = androides + iphones + otros;
    estadistica1 = androides*todos;
    estadistica1 = estadistica1.toFixed(2);

    estadistica2 = iphones*todos;
    estadistica2 = estadistica2.toFixed(2);

    estadistica3 = otros*todos;
    estadistica3 = estadistica3.toFixed(2);
    
    tfHTML1 = "<tr><td>" + "" + "</td><td>" + "androides" + "</td><td>" + estadistica1 + "%"  + "</tr></td>";
    $("#tfoot_class").append(tfHTML1);
    tfHTML2 = "<tr><td>" + "" + "</td><td>" + "iphones" + "</td><td>" + estadistica2 + "%" + "</tr></td>";
    $("#tfoot_class").append(tfHTML2);
    tfHTML3 = "<tr><td>" + "" + "</td><td>" + "otros" + "</td><td>" + estadistica3 + "%"  + "</tr></td>";
    $("#tfoot_class").append(tfHTML3);
});
});

//Ejercicio 1 Ajax, parte 1 ^

$("button.boton5").click(function () {
    id_pedido = $("#numeroID").val();
    console.log(id_pedido);
    id_parse = parseInt(id_pedido);
    console.log(id_parse);
    if (isNaN(id_parse)) {
        $("#numeroID").css("background-color", "red");
    }
    if(id_parse > 10 || id_parse < 1) {
        $("#numeroID").css("background-color", "red");
    } else {
        $.getJSON("Json/clientes.json", function (data) {
            data.clientes.forEach(function (cliente) {
                if(cliente.id == id_parse) {
                    console.log("Cliente ID " + cliente.id);
                    thHTML = "<tr><th>" + "ID" + "</th><td>" + cliente.id + "</td></tr>" +
                    "<tr><th>" + "Nombre" + "</th><td>" + cliente.nombre + "</td></tr>" +
                    "<tr><th>" + "Teléfono" + "</th><td>" + cliente.tel + "</td></tr>" +
                    "<tr><th>" + "Email" + "</th><td>" + cliente.email + "</td></tr>";
                    $("#tabla_id").append(thHTML);
                }
            });
        });
    }
});
